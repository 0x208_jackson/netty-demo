package com.example.netty.nbcb.client;

import com.example.netty.common.client.ClientHeartBeatServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:06
 **/
@SuppressWarnings("all")
public class NormalNBCBClientInitializer extends ChannelInitializer<SocketChannel> {

    private static final String DECODER = "decoder";
    private static final String ENCODER = "encoder";
    /**
     * 为读超时时间（即多长时间没有接受到客户端发送数据）
     */
    private final long readerIdleTime = 0;
    /**
     * 为写超时时间（即多长时间没有向客户端发送数据）
     */
    private final long writerIdleTime = 0;
    /**
     * 所有类型（读或写）的超时时间
     */
    private final long allIdleTime = 0;

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new ClientHeartBeatServerHandler())
                .addLast(new IdleStateHandler(readerIdleTime, writerIdleTime, allIdleTime, TimeUnit.SECONDS))
                .addLast(new NormalNBCBClientHandler())
        ;
    }
}
