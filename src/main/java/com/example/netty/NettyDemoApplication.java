package com.example.netty;

import com.example.netty.common.client.NettyClient;
import com.example.netty.common.server.NettyServer;
import com.example.netty.common.utils.NettyConstants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.annotation.Resource;

@SpringBootApplication
@EnableAsync
public class NettyDemoApplication {

    @Resource
    private NettyServer nettyServer;
    @Resource
    private NettyClient nettyClient;

    public static void main(String[] args) {
        SpringApplication.run(NettyDemoApplication.class, args);
        System.out.println("+++++服务启动成功+++++");
    }

    @EventListener(ApplicationStartedEvent.class)
    public void init() {
        // 测试 normal byteBuf 包
//        String type = NettyConstants.NORMAL_BYTE_BUF;
        // 测试 byte 包
//        String type = NettyConstants.NORMAL_BYTE;
        // 测试String 包
//        String type = NettyConstants.NORMAL_STRING;
        // 演示 拆包和粘包
//        String type = NettyConstants.NBCB;
        // 粘包和拆包固定长度解决方案
//        String type = NettyConstants.FIX_LENGTH;
        // 拆包、粘包 结尾\r\n 来区分整包解决方案
//        String type = NettyConstants.LINE_BASE;
        // 拆包、粘包 特殊字符结尾来区分整包解决方案
//        String type = NettyConstants.DELIMITER;
        // 头部设置 整包的长度来进行整包区分，每个包的长度放在头部
        String type = NettyConstants.LENGTH_FIELD;
        nettyServer.init("127.0.0.1", 9000, type);
        nettyClient.init(type);
    }
}
