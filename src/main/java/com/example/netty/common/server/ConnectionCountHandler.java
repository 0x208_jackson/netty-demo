package com.example.netty.common.server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:21
 **/
@ChannelHandler.Sharable
public class ConnectionCountHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ConnectionCountHandler.class);

    /**
     * 每次过来一个新连接就对连接数加一
     *
     * @param ctx ChannelHandlerContext
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.debug("有新的链接加入：{}", ctx.channel().id().asLongText());
        super.channelActive(ctx);
    }

    /**
     * 断开的时候减一
     *
     * @param ctx ChannelHandlerContext
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        logger.debug("有连接断开：{}", ctx.channel().id().asLongText());
        super.channelInactive(ctx);
    }
}
