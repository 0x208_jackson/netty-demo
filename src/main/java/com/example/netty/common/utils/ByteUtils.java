package com.example.netty.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/3 16:00
 **/
public class ByteUtils {
    private static final Logger logger = LoggerFactory.getLogger(ByteUtils.class);

    public static byte[] append(byte[] destBytes, byte[] srcBytes) {
        int destLength = destBytes.length;
        int srcLength = srcBytes.length;
        int newCapacity = srcLength + destLength;
        destBytes = Arrays.copyOf(destBytes, newCapacity);
        System.arraycopy(srcBytes, 0, destBytes, destLength, srcLength);
        return destBytes;
    }
}
