package com.example.netty.common.utils;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/3 10:41
 **/
public interface NettyConstants {

    String NORMAL_BYTE_BUF = "normalBytBuf";
    String NORMAL_BYTE = "normalByte";
    String NORMAL_STRING = "normalString";
    String NBCB = "nbcb";
    String FIX_LENGTH = "fixLength";
    String LINE_BASE = "lineBase";
    String DELIMITER = "delimiter";
    String LENGTH_FIELD = "lengthField";


    /**
     * 特殊字符作为分隔符来区分整包消息； 服务端接收客户端消息，分割字符
     */
    String DELIMITER_SPLIT_SERVER = "78B987";
    /**
     * 特殊字符作为分隔符来区分整包消息； 客户端接收服务端消息，分割字符
     */
    String DELIMITER_SPLIT_CLIENT = ";";
}
