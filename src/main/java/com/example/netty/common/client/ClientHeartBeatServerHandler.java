package com.example.netty.common.client;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:23
 **/
public class ClientHeartBeatServerHandler extends ChannelInboundHandlerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(ClientHeartBeatServerHandler.class);

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state().equals(IdleState.ALL_IDLE)) {
                logger.info("长时间没有进行过读写操作,发送查询状态的心跳");
                // 15 分钟没有进行读操作，给服务端发送心跳包
//                ctx.channel().writeAndFlush(BACK_FLOW_ORDER_QUERY_STATUS);
            } else if (event.state().equals(IdleState.READER_IDLE)) {
                logger.info("长时间没有进行过读操作");
            } else if (event.state().equals(IdleState.WRITER_IDLE)) {
                logger.info("长时间没有进行过写操作");
            }
        }
    }
}
