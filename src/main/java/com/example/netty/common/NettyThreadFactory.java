package com.example.netty.common;

import org.springframework.lang.NonNull;

import java.util.concurrent.ThreadFactory;

/**
 * @author xin
 * @version Created by xin on 2021/6/16 10:44 下午
 */
public class NettyThreadFactory implements ThreadFactory {

    private final String threadName;

    public NettyThreadFactory(String threadName) {
        this.threadName = threadName;
    }

    @Override
    public Thread newThread(@NonNull Runnable r) {
        Thread thread = new Thread(r);
        // 设置守护线程
        thread.setDaemon(true);
        thread.setName(threadName + "-thread-" + thread.getId());
        return thread;
    }
}
