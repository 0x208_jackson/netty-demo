package com.example.netty.lengthfield.client;

import com.example.netty.common.client.ClientHeartBeatServerHandler;
import com.example.netty.common.utils.NettyConstants;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:06
 **/
@SuppressWarnings("all")
public class LengthFieldClientInitializer extends ChannelInitializer<SocketChannel> {
    private static final String DECODER = "decoder";
    private static final String ENCODER = "encoder";
    private static final String PING = "ping";
    /**
     * 长度字段偏移位置为0表示从包的第一个字节开始读取；
     */
    private final int lengthFieldOffset = 0;
    /**
     * 长度字段长为2，从包的开始位置往后2个字节的长度为长度字段；
     */
    private final int lengthFieldLength = 4;
    /**
     * 解析的时候无需跳过任何长度；
     */
    private final int lengthAdjustment = 0;
    /**
     * 无需去掉当前数据包的开头字节数, header + body
     */
    private final int initialBytesToStrip = 4;
    /**
     * 如果failFast=true，当超过maxLength后会立刻抛出TooLongFrameException，不再进行解码；
     * 如果failFast=false，那么会等到解码出一个完整的消息后才会抛出TooLongFrameException
     */
    private final boolean failFast = false;
    /**
     * 为读超时时间（即多长时间没有接受到客户端发送数据）
     */
    private final long readerIdleTime = 0;
    /**
     * 为写超时时间（即多长时间没有向客户端发送数据）
     */
    private final long writerIdleTime = 0;
    /**
     * 所有类型（读或写）的超时时间
     */
    private final long allIdleTime = 0;

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new ClientHeartBeatServerHandler())
                .addLast(new IdleStateHandler(readerIdleTime, writerIdleTime, allIdleTime, TimeUnit.SECONDS))
                // 优先判断最大长度
                // stripDelimiter:解码后的消息是否去除分隔符 不去掉分割符
                // failFast = false, 那么会等到解码出完整消息才会抛出 TooLongException。
                // 对应的是server 的发送的配置，默认给前端添加了 4个字节作为长度，
                // 因此，长度起始位是 从0 开始，长度所占的字节是4个，解析的时候 需要 跳过 4个字节，将添加的长度去掉
                .addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0,
                        4, 0, 0,false))
                .addLast(ENCODER, new ByteArrayEncoder())
                .addLast(DECODER, new StringDecoder())
                .addLast(new LengthFieldClientHandler())
        ;
    }
}
