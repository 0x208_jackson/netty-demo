package com.example.netty.normal.string.client;

import com.example.netty.common.client.ClientHeartBeatServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:06
 **/
@SuppressWarnings("all")
public class NormalStringClientInitializer extends ChannelInitializer<SocketChannel> {

    /**
     * 为读超时时间（即多长时间没有接受到客户端发送数据）
     */
    private final long readerIdleTime = 0;
    /**
     * 为写超时时间（即多长时间没有向客户端发送数据）
     */
    private final long writerIdleTime = 0;
    /**
     * 所有类型（读或写）的超时时间
     */
    private final long allIdleTime = 0;
    private static final String DECODER = "decoder";
    private static final String ENCODER = "encoder";
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        // 心跳监听配置
        ch.pipeline().addLast(new ClientHeartBeatServerHandler())
                // 心跳监听配置
                .addLast(new IdleStateHandler(readerIdleTime, writerIdleTime, allIdleTime, TimeUnit.SECONDS))
                // 客户端向服务端发送消息时 采用 字符串 编码方式，对应 NormalStringServerInitializer 中的decoder
                // TestClientController.testNormalString 中发送时需要 以字符串方式进行发送
                // NormalStringServerHandler.channelRead 以字符串方式进行接收
                .addLast(ENCODER, new StringEncoder())
                // 服务端向客户端发送消息，采用字符串编码方式，对应 NormalStringServerInitializer 中的encoder
                // 需要对应 NormalStringServerHandler.channelRead  ctx.channel().writeAndFlush(serverMsg);
                // NormalStringClientHandler.channelRead 通过字符串接收
                .addLast(DECODER, new StringDecoder())
                // 用于监听消息、处理消息，如果 DECODER 设置的是字节，则channelRead 收到的msg 是字符串
                .addLast(new NormalStringClientHandler())

        ;
    }
}
