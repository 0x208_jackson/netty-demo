package com.example.netty.normal.string.client;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:41
 **/
public class NormalStringClientHandler extends ChannelInboundHandlerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(NormalStringClientHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        logger.info("接收到服务端的响应:{} ", ctx.channel().id().asLongText());
        // 服务端向客户端发送消息，采用字符串编码方式，对应 NormalStringServerInitializer 中的encoder
        // NormalStringClientInitializer 的decoder  StringDecoder 将字节转 字符串处理
        // 因此此处采用 字符串接收，
        String body = (String) msg;
        System.out.println("-----client start------| " + body + " | ------client end------");
    }
}
