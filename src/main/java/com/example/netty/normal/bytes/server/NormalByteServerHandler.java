package com.example.netty.normal.bytes.server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:39
 **/
public class NormalByteServerHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(NormalByteServerHandler.class);


    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        logger.debug("收到客户端消息");
        try {
            // 因为TestClientController.testNormalByte 中发送时以字节发送
            // 对应NormalByteServerInitializer 的decoder ByteArrayDecoder 此类将 byteBuf 处理为了 bytes
            // NormalByteClientInitializer encoder
            // 因此此处可以强转为 byte
            String body = new String((byte[]) msg, StandardCharsets.UTF_8);
            System.out.println("-----server start------| " + body + " | ------server end------");

            //发送给客户端
            String serverMsg = "++++++++我是服务端给客户端的消息+++++++";
            // 此处需要对应 NormalByteServerInitializer 中的encoder 模式
            // 以及 NormalByteClientInitializer 中的decoder 模式，如果不匹配则客户端收不到消息
            // 因为 encoder 与 decoder 配置的是 ByteArrayDecoder、ByteArrayEncoder 因此此处只能发送字节
            ctx.channel().writeAndFlush(serverMsg.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            logger.error("接收数据异常", e);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
