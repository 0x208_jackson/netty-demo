package com.example.netty.normal.bytes.client;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:41
 **/
public class NormalByteClientHandler extends ChannelInboundHandlerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(NormalByteClientHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        logger.info("接收到服务端的响应:{} ", ctx.channel().id().asLongText());
        String body = new String((byte[]) msg, StandardCharsets.UTF_8);
        System.out.println("-----client start------| " + body + " | ------client end------");
    }
}
