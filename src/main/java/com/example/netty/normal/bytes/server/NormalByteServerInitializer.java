package com.example.netty.normal.bytes.server;

import com.example.netty.common.server.ConnectionCountHandler;
import com.example.netty.common.server.ServerHeartBeatServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:04
 **/
@SuppressWarnings("all")
public class NormalByteServerInitializer extends ChannelInitializer<SocketChannel> {
    private static final String ENCODER = "encoder";
    private static final String DECODER = "decoder";

    /**
     * 为读超时时间（即多长时间没有接受到客户端发送数据）
     */
    private final long readerIdleTime = 0;
    /**
     * 为写超时时间（即多长时间没有向客户端发送数据）
     */
    private final long writerIdleTime = 0;
    /**
     * 所有类型（读或写）的超时时间
     */
    private final long allIdleTime = 0;

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new IdleStateHandler(readerIdleTime, writerIdleTime, allIdleTime, TimeUnit.SECONDS))
                // 服务端向客户端发送消息，采用byte 编码方式，对应 NormalByteClientInitializer 中的decoder
                // 需要对应 NormalByteServerHandler.channelRead  ctx.channel().writeAndFlush(serverMsg.getBytes(StandardCharsets.UTF_8));
                .addLast(ENCODER, new ByteArrayEncoder())
                // 客户端向服务端发送消息时 采用 byte 编码方式，对应 NormalByteClientInitializer 中的 encoder
                // TestClientController.testNormalByte 中发送时需要 以 字节方式进行发送
                .addLast(DECODER, new ByteArrayDecoder())
                // 监听消息，并接收消息，此处配置的是 字节，因此收到的消息 是字节，强转位 字节
                .addLast(new NormalByteServerHandler())
                // 监听 客户端连接
                .addLast(new ConnectionCountHandler())
                // 监听客户端心跳
                .addLast(new ServerHeartBeatServerHandler())
        ;

    }
}
