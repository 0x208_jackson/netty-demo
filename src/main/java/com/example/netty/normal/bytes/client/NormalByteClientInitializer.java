package com.example.netty.normal.bytes.client;

import com.example.netty.common.client.ClientHeartBeatServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:06
 **/
@SuppressWarnings("all")
public class NormalByteClientInitializer extends ChannelInitializer<SocketChannel> {

    /**
     * 为读超时时间（即多长时间没有接受到客户端发送数据）
     */
    private final long readerIdleTime = 0;
    /**
     * 为写超时时间（即多长时间没有向客户端发送数据）
     */
    private final long writerIdleTime = 0;
    /**
     * 所有类型（读或写）的超时时间
     */
    private final long allIdleTime = 0;
    private static final String DECODER = "decoder";
    private static final String ENCODER = "encoder";
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new ClientHeartBeatServerHandler())
                // 心跳监听配置
                .addLast(new IdleStateHandler(readerIdleTime, writerIdleTime, allIdleTime, TimeUnit.SECONDS))
                // 客户端向服务端发送消息时 采用 byte 编码方式，对应 NormalByteServerInitializer 中的decoder
                // TestClientController.testNormalByte 中发送时需要 以 字节方式进行发送
                .addLast(ENCODER, new ByteArrayEncoder())
                // 服务端向客户端发送消息，采用byte 编码方式，对应NormalByteServerInitializer 中的encoder
                // 需要对应 NormalByteServerHandler.channelRead  ctx.channel().writeAndFlush(serverMsg.getBytes(StandardCharsets.UTF_8));
                .addLast(DECODER, new ByteArrayDecoder())
                // 用于监听消息、处理消息，如果 DECODER 设置的是字节，则channelRead 收到的msg 是字节
                .addLast(new NormalByteClientHandler())
        ;
    }
}
