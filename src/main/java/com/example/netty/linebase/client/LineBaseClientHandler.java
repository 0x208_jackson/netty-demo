package com.example.netty.linebase.client;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:41
 **/
public class LineBaseClientHandler extends ChannelInboundHandlerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(LineBaseClientHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        logger.info("接收到服务端的响应");
        String body = (String) msg;
        System.out.println("-----client start------| \n " + body + " \n| ------client end------");
    }
}
