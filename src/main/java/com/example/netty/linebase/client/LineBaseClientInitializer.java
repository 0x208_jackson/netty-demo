package com.example.netty.linebase.client;

import com.example.netty.common.client.ClientHeartBeatServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:06
 **/
@SuppressWarnings("all")
public class LineBaseClientInitializer extends ChannelInitializer<SocketChannel> {
    private static final String DECODER = "decoder";
    private static final String ENCODER = "encoder";

    /**
     * 为读超时时间（即多长时间没有接受到客户端发送数据）
     */
    private final long readerIdleTime = 0;
    /**
     * 为写超时时间（即多长时间没有向客户端发送数据）
     */
    private final long writerIdleTime = 0;
    /**
     * 所有类型（读或写）的超时时间
     */
    private final long allIdleTime = 0;

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new ClientHeartBeatServerHandler())
                .addLast(new IdleStateHandler(readerIdleTime, writerIdleTime, allIdleTime, TimeUnit.SECONDS))
                // 优先判断最大长度
                // stripDelimiter:解码后的消息是否去除分隔符 此处是 通过 \n\r来进行区分，因此接收到消息后需要去掉分割符
                // failFast = false, 那么会等到解码出完整消息才会抛出 TooLongException。
//                Integer.MAX_VALUE 一包的最大长度，如果包长大于这个最大长度则会抛出异常
                .addLast(new LineBasedFrameDecoder(Integer.MAX_VALUE, true, false))
                .addLast(ENCODER, new StringEncoder())
                .addLast(DECODER, new StringDecoder())
                .addLast(new LineBaseClientHandler())

        ;
    }
}
