package com.example.netty.fixlength.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:39
 **/
public class FixLengthServerHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(FixLengthServerHandler.class);


    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        logger.debug("收到客户端消息");
        try {
            String body = (String) msg;
            System.out.println("-----server start------| \n " + body + " \n| ------server end------");
            // 此处  我是服务端给客户端的消息;  字节长度是 37； FixLengthClientInitializer FixedLengthFrameDecoder 配置的是 37,因此客户端收到 两条数据
            ctx.channel().writeAndFlush("我是服务端给客户端的消息;我是服务端给客户端的消息;");
        } catch (Exception e) {
            logger.error("接收数据异常", e);
        }
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

    public static void main(String[] args) {
        System.out.println("我是服务端给客户端的消息;".getBytes(StandardCharsets.UTF_8).length);
    }
}
