package com.example.netty.fixlength.server;

import com.example.netty.common.server.ConnectionCountHandler;
import com.example.netty.common.server.ServerHeartBeatServerHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * <p></p>
 *
 * @author xin
 * @version 2023/11/2 16:04
 **/
@SuppressWarnings("all")
public class FixLengthServerInitializer extends ChannelInitializer<SocketChannel> {
    private static final String ENCODER = "encoder";
    private static final String DECODER = "decoder";

    /**
     * 为读超时时间（即多长时间没有接受到客户端发送数据）
     */
    private final long readerIdleTime = 0;
    /**
     * 为写超时时间（即多长时间没有向客户端发送数据）
     */
    private final long writerIdleTime = 0;
    /**
     * 所有类型（读或写）的超时时间
     */
    private final long allIdleTime = 0;

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new IdleStateHandler(readerIdleTime, writerIdleTime, allIdleTime, TimeUnit.SECONDS))
                // 固定 长度， 每一包消息长度必须是 64 字节，只有达到 64 时才会被接收
//                所有的消息，必须是 64 的倍数，不然会截断产生乱码，
//                发送的时候   TestClientController.testFixLength0  需要发送 32 字节
                .addLast(new FixedLengthFrameDecoder(32))
                .addLast(ENCODER, new StringEncoder())
                .addLast(DECODER, new StringDecoder())
                .addLast(new ConnectionCountHandler())
                .addLast(new FixLengthServerHandler())
                .addLast(new ServerHeartBeatServerHandler())

        ;

    }
}
